package com.example.notes.Model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Notes")
class Notes(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var title: String,
    var subTitle: String,
    var note: String,
    var date: String,
    var priority: String,
    var notesData: String,


    ) {
    override fun toString(): String {
        return super.toString()
    }
}
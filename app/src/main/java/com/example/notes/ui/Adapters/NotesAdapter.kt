package com.example.notes.ui.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notes.Model.Notes
import com.example.notes.R

class NotesAdapter(private val mList: List<Notes>) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_note_item, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val notesModel = mList[position]
        holder.tvTitle.text = notesModel.title
        holder.tvSubtitle.text = notesModel.subTitle
        holder.tvNotes.text = notesModel.notesData
        holder.tvDate.text = "Date : ${notesModel.date}"
        if (notesModel.priority == "1") {
            holder.ivGreen.visibility = View.VISIBLE
            holder.ivYellow.visibility = View.GONE
            holder.ivRed.visibility = View.GONE
        } else if (notesModel.priority == "2") {
            holder.ivGreen.visibility = View.GONE
            holder.ivYellow.visibility = View.VISIBLE
            holder.ivRed.visibility = View.GONE
        } else if (notesModel.priority == "3") {
            holder.ivGreen.visibility = View.GONE
            holder.ivYellow.visibility = View.GONE
            holder.ivRed.visibility = View.VISIBLE
        } else {
            holder.ivGreen.visibility = View.GONE
            holder.ivYellow.visibility = View.GONE
            holder.ivRed.visibility = View.GONE
        }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvSubtitle: TextView = itemView.findViewById(R.id.tvSubtitle)
        val tvNotes: TextView = itemView.findViewById(R.id.tvNotes)
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)

        val ivGreen: ImageView = itemView.findViewById(R.id.ivGreen)
        val ivYellow: ImageView = itemView.findViewById(R.id.ivYellow)
        val ivRed: ImageView = itemView.findViewById(R.id.ivRed)
    }
}
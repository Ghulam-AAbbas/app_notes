package com.example.notes.ui.Fragments

import android.os.Bundle
import android.text.Editable
import android.text.format.DateFormat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.notes.R
import com.example.notes.ViewModel.NotesViewModel
import com.example.notes.databinding.FragmentCreateNotesBinding
import java.util.*
import androidx.lifecycle.ViewModelProvider
import com.example.notes.Model.Notes


class CreateNotesFragment : Fragment() {

    lateinit var binding: FragmentCreateNotesBinding
    var priority: String = "1"


    lateinit var viewModel: NotesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = FragmentCreateNotesBinding.inflate(layoutInflater, container, false)
        viewModel = ViewModelProvider(this).get(NotesViewModel::class.java)


        binding.pGreen.setImageResource(R.drawable.ic_done)

        binding.pGreen.setOnClickListener {
            priority = "1"
            binding.pGreen.setImageResource(R.drawable.ic_done)
            binding.pRed.setImageResource(0)
            binding.pYellow.setImageResource(0)
        }
        binding.pYellow.setOnClickListener {
            priority = "2"
            binding.pYellow.setImageResource(R.drawable.ic_done)
            binding.pRed.setImageResource(0)
            binding.pGreen.setImageResource(0)
        }
        binding.pRed.setOnClickListener {
            priority = "3"
            binding.pRed.setImageResource(R.drawable.ic_done)
            binding.pGreen.setImageResource(0)
            binding.pYellow.setImageResource(0)
        }


        binding.btnSaveNotes.setOnClickListener {
            createNotes(it)
        }


        return binding.root
    }

    private fun createNotes(it: View?) {
        val title = binding.editTitle.text
        val subTitle = binding.editSubtitle.text
        val notes = binding.editNotes.text
        val d = Date()
        val notesDate: CharSequence = DateFormat.format("MMMM d, yyyy HH:mm", d.time)

        val list = Notes(
            title = title.toString(),
            subTitle = subTitle.toString(),
            notesData = notes.toString(),
            date = notesDate.toString(),
            priority = priority,
            note = ""
        )
        viewModel.addNotes(list)
        Toast.makeText(requireContext(), "Notes Created Successfully", Toast.LENGTH_LONG).show()
        activity?.onBackPressed()
    }
}

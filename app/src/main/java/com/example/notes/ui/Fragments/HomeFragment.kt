package com.example.notes.ui.Fragments

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notes.Model.Notes
import com.example.notes.R
import com.example.notes.ViewModel.NotesViewModel
import com.example.notes.databinding.ActivityMainBinding
import com.example.notes.databinding.FragmentHomeBinding
import com.example.notes.ui.Adapters.NotesAdapter

class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding

    //    val viewModel: NotesViewModel by viewModels()
    lateinit var viewModel: NotesViewModel
    val notesList = ArrayList<Notes>()
    lateinit var notesAdapter: NotesAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        viewModel = ViewModelProvider(this).get(NotesViewModel::class.java)


        binding.btnAddNotes.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(R.id.action_homeFragment_to_createNotesFragment)
        }

        setUpRecyclerView()
        viewModel.getNotes().observe(viewLifecycleOwner, {
            notesList.clear()
            notesList.addAll(it)
            setUpRecyclerView()
            it.forEach {
                Log.wtf("forEach","${it.toString()}")
            }
        })
        return binding.root
    }

    fun setUpRecyclerView() {
        binding.rcvAllNotes.layoutManager = LinearLayoutManager(requireContext())
        notesAdapter = NotesAdapter(notesList)
        binding.rcvAllNotes.adapter = notesAdapter
    }
}